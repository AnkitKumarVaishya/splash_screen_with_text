import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
            body: Center(
        child:  Shimmer.fromColors(
          baseColor: Color(0xff7f00ff),
          highlightColor: Color(0xffe100ff),
          child: Container(
            padding: EdgeInsets.all(16.0),
            child: Text(
              "Page 2",
              style: TextStyle(
                  fontSize: 100.0,
                  fontFamily: 'Pacifico',
                  shadows: <Shadow>[
                    Shadow(
                        blurRadius: 18.0,
                        color: Colors.black,
                        offset: Offset.fromDirection(120, 12))
                  ],
                  ),
            ),
          ),
        ),
      ),
    );
  }
}
