import 'package:flutter/material.dart';
import 'package:splashtext_screen/Splash_screen.dart';

import 'Home_page.dart';

void main() {
  runApp(MyApp());
}
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
            debugShowCheckedModeBanner: false,
      home: Splash(),
      
    );
  }
}
