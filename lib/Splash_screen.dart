import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:splashtext_screen/Home_page.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState(){
    super.initState();
    mockCheckForSession().then(
      (status){
        if(status){
          navigateToHome();
        }else{
          navigateToHome();
        }
      }
    );
  }
  Future<bool> mockCheckForSession() async {
    await Future.delayed(Duration(seconds: 5), () {});
    return true;
  }

  void navigateToHome() {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (BuildContext context)=> Home(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Shimmer.fromColors(
          baseColor: Color(0xff7f00ff),
          highlightColor: Color(0xffe100ff),
          child: Container(
            
            padding: EdgeInsets.all(16.0),
            child: Text(
              "Splash Screen",
              style: TextStyle(
                  fontSize: 100.0,
                  fontFamily: 'Pacifico',
                  shadows: <Shadow>[
                    Shadow(
                        blurRadius: 18.0,
                        color: Colors.black87,
                        offset: Offset.fromDirection(120, 12))
                  ]),
            ),
          ),
        ),
      ),
    );
  }
}
